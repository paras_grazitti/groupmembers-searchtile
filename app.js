

var express = require('express'),
     http = require('http'),
     https = require('https');

var app = express();


var options = {

    key: fs.readFileSync('/usr/local/ssl/certificates/connectors-grazitti-com.key'),

    cert: fs.readFileSync('/usr/local/ssl/certificates/connectors-grazitti-com.crt'),

    ca: [ fs.readFileSync('/usr/local/ssl/certificates/intermediate.crt') ]

};

app
    .use('/group-members-view', require('./group-members-view/server').app)
     .use('/search-tile', require('./search-tile/server').app);

var server = http.createServer(options,app).listen(5700, app.get('hostname')|| 'localhost', function() {
    console.log("Express server listening on " + server.address().address + ':' + server.address().port);
});

