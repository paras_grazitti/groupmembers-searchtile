
var jiveBaseUrl =  "https://searchtile-bit9uex.rhcloud.com/cbSearchTile";
jive.tile.onOpen(function (config, options) {
    //gadgets.window.adjustWidth();
    console.log("Ready function");
    osapi.jive.core.get({
        v: "v3",
        href: '/people'
    }).execute(function (response)  {
        console.log("People Result",response);
        var authors = document.getElementById('Authors');
        for(i=0;i<response.content.list.length;i++){

            var newDiv = document.createElement('div');
            newDiv.className = "radio1";
            var newOption = document.createElement('input');
            newOption.style.width  ="12px"
            var label = document.createElement('label');
            label.className = "labelpadding";
            var span = document.createElement('span');
            span.className = "labelpadding1";
            // var brtag = document.createElement('br');
            //newOption.style.width = "20px";
            newOption.id = response.content.list[i].id;
            newOption.onclick = function() { checkedf1(); };
            newOption.value = response.content.list[i].displayName;
            span.innerHTML = response.content.list[i].displayName;
            label.id = "A" + i;
            newOption.type = "checkbox";
            label.appendChild(newOption);
            label.appendChild(span);
            newDiv.appendChild(label);

            authors.appendChild(newDiv);

        }

        gadgets.window.adjustHeight();
    });


    osapi.http.get({
        href: jiveBaseUrl + "/configure/selectedSpaces",
        'refreshInterval': 0,
        format: 'json',
        'authz': 'signed',
        'headers': {
            'Content-Type': ['application/json']
        }

    }).execute(function (response) {
        console.log("Spaces Response",response);
        var places = document.getElementById('places');
        for(i=0;i<response.content.spaces.length;i++){
            var newDiv = document.createElement('div');
            // newDiv.className = "checkbox";
            var newOption = document.createElement('input');
            newOption.style.width  ="12px"
            var label = document.createElement('label');
            label.className = "labelpadding";
            var span = document.createElement('span');
            span.className = "labelpadding1";
            //newOption.style.width = "20px";
            // var brtag = document.createElement('br');
            newOption.onclick = function() { searchQuery(); };
            newOption.id = response.content.spaceId[i];
            newOption.value = response.content.spaces[i];
            span.innerHTML = response.content.spaces[i];
            label.id = "A" + i;
            newOption.type = "checkbox";
            label.appendChild(newOption);
            label.appendChild(span);

            newDiv.appendChild(label);

            places.appendChild(newDiv);

        }

    });


});
function checkedf(){
    $( document ).ready(function() {
        console.log("Inside");
        $('.radiob input:checkbox').click(function() {
            console.log("Checkbox");
            $('.radiob input:checkbox').not(this).prop('checked', false);

        });
        searchQuery();
    });

}

function checkedf1(){
    $( document ).ready(function() {
        console.log("Inside");
        $('.radio1 input:checkbox').click(function() {
            console.log("Checkbox");
            $('.radio1 input:checkbox').not(this).prop('checked', false);

        });
        searchQuery();
    });

}


function searchQuery() {
    document.getElementById("loader").style.display = "block";
    document.getElementById("searchedContent").style.display = "none";
    document.getElementById('pagin').style.display = "none";

    // var pagin = document.getElementById('pagination');
    // if (pagin.children.length > 0){
    //     while (pagin.hasChildNodes()) {
    //         pagin.removeChild(pagin.firstChild);
    //     }
    // }

    var authors = document.getElementById('Authors');
    var places = document.getElementById('places');
    var modifiedF =  document.getElementById('modified')
    var peopleId ="";
    var spaceId = "";
    var modified = "";
    for( var a =0;a<authors.children.length;a++ ){
        if(authors.children[a].children[0].children[0].checked == true){

            peopleId ="/people/" + authors.children[a].children[0].children[0].id ;
            console.log("Inside if condition PlaceId",peopleId)
        }
    }
    console.log("peopleId in for loop" ,peopleId);
    for(var s=0;s<places.children.length;s++){
        if(places.children[s].children[0].children[0].checked == true){
            spaceId = spaceId + "/places/" + places.children[s].children[0].children[0].id + ",";
            console.log("Space Id",spaceId)
        }
    }
    for(var m=0;m<modifiedF.children.length;m++){
        if(modifiedF.children[m].children[0].children[0].checked == true){
            if(modifiedF.children[m].children[0].value=="All time"){
                var currentTime = new Date(0);
                modified = currentTime.toISOString();
            }
            else if(modifiedF.children[m].children[0].children[0].value=="1 day"){
                var currentTime = new Date().valueOf();
                currentTime = currentTime - 86400000;
                modified = new Date(currentTime).toISOString();

            }
            else if(modifiedF.children[m].children[0].children[0].value=="7 days"){
                var currentTime = new Date().valueOf();
                currentTime = currentTime - 86400000*7;
                modified = new Date(currentTime).toISOString();
            }
            else if(modifiedF.children[m].children[0].children[0].value=="30 days"){
                var currentTime = new Date().valueOf();
                currentTime = currentTime - 86400000*30;
                modified = new Date(currentTime).toISOString();
            }
            else if(modifiedF.children[m].children[0].children[0].value=="90 days"){
                var currentTime = new Date().valueOf();
                currentTime = currentTime - 86400000*90;
                modified = new Date(currentTime).toISOString();

            }
            else if(modifiedF.children[m].children[0].children[0].value=="1 year"){
                var currentTime = new Date().valueOf();
                currentTime = currentTime - 86400000*365;
                modified = new Date(currentTime).toISOString();

            }

        }
    }



    var searchItem = document.getElementById('searchItem').value;
    if(searchItem != "" ){
        searchItem +="*";
        console.log("Search Item");
        document.getElementById('noResult').style.display = "none";

        if(peopleId!="" && spaceId !="" && modified != "" ){
            console.log("C1");

            var request = osapi.jive.corev3.contents.search({search:searchItem ,
                    author:peopleId,
                    place:spaceId,
                    after:modified});

                searchingData(request);

        }
        else if (peopleId!="" && spaceId !="" && modified == ""){
            console.log("C2");

            var request = osapi.jive.corev3.contents.search({search:searchItem,
                    author:peopleId,
                    place:spaceId});

            searchingData(request);

        }
        else if(peopleId!="" && spaceId =="" && modified != ""){
            console.log("C3");

            var request = osapi.jive.corev3.contents.search({search:searchItem,
                    author:peopleId,
                    after:modified});

            searchingData(request);

        }
        else if (peopleId =="" && spaceId !="" && modified != ""){
            console.log("C4");

            var request = osapi.jive.corev3.contents.search({search:searchItem,
                    place:spaceId,
                    after:modified});

            searchingData(request);

        }
        else if(peopleId!="" && spaceId =="" && modified == "" ){
            console.log("C5");

            var request = osapi.jive.corev3.contents.search({search:searchItem,
                    author:peopleId});

            searchingData(request);
        }
        else if(peopleId=="" && spaceId !="" && modified == "" ){
            console.log("C6");

            var request = osapi.jive.corev3.contents.search({search:searchItem,
                    place:spaceId});

            searchingData(request);

        }
        else if(peopleId=="" && spaceId =="" && modified != "" ){
            console.log("C7");

            var request = osapi.jive.corev3.contents.search({search:searchItem,
                    after:modified});

            searchingData(request);

        }
        else{
            console.log("C8");

            var request = osapi.jive.corev3.contents.search({search:searchItem});

            searchingData(request);

        }
    }
    else{

        document.getElementById("loader").style.display = "none";
        document.getElementById('noResult').style.display = "block";
        document.getElementById('pagin').style.display = "none";
        gadgets.window.adjustHeight();

    }
    // showPage();


}

function searchingData(request){
    var contentResponse =[];
    var req = request;
    searchingDataI(req);
    function searchingDataI(req){
        //console.log("Request",request);

        req.execute(function (response)  {
            //console.log("People Result",response);


            if (response.error) {
                var code = response.error.code;
                var message = response.error.message;
                // present the user with an appropriate error message
            } else if (!response.list) {
                alert("Error: response is not a list!");
            } else {
                response.list.forEach(function(spaces){
                    contentResponse.push(spaces);
                    // console.log(SpaceResponse);

                });
                if (response.getNextPage) {
                    var requestNextPage = response.getNextPage();
                    searchingDataI(requestNextPage);
                }
                else{
                    console.log("contentResponse ",contentResponse)

                    if (contentResponse.length > 0){


                            $(function () {
                                var pagin = document.getElementById('pagination');
                                console.log("Inside twsPagination");
                                console.log("Pagination children",pagin.children.length);
                                $('#pagination').twbsPagination('destroy');
                                 $('#pagination').twbsPagination({
                                    totalPages: Math.ceil(contentResponse.length / 7),
                                    visiblePages: 2,
                                    onPageClick: function (event, page) {
                                        console.log("Event",event);
                                        var searchedContent = document.getElementById('searchedContent');
                                        if (searchedContent.children.length > 0){
                                            while (searchedContent.hasChildNodes()) {
                                                searchedContent.removeChild(searchedContent.firstChild);
                                            }
                                        }

                                        console.log("current page",page);
                                        paginatedContent(page,contentResponse);

                                    }
                                });

                            });


                        function paginatedContent(currentPage,contentResponse){
                            var records_per_page =7 ;
                            if(parseInt(contentResponse.length) - parseInt((currentPage-1)*7) < 7){
                                var pageSize = parseInt(contentResponse.length) - parseInt((currentPage-1)*7);
                                console.log("If condtion");
                                for(i=(currentPage-1)*records_per_page;i<contentResponse.length;i++){
                                    var iconspan = document.createElement('span');
                                    iconspan.className = "pwcmps_Icon " + contentResponse[i].iconCss;
                                    var newDiv = document.createElement('div');
                                    var bodyspan = document.createElement('span');
                                    var ref = document.createElement('a');
                                    var refQ = document.createElement('a');
                                    var days = numberofDays(contentResponse[i].updated);
                                    // var link = contentResponse[i].author.thumbnailUrl;
                                    // link = link.substring(0,35);
                                    ref.href  = contentResponse[i].resources.html.ref;
                                    ref.target ="_blank";

                                    refQ.href  = contentResponse[i].resources.html.ref;
                                    refQ.target ="_blank";

                                    var nameref = document.createElement('a');
                                    nameref.href = contentResponse[i].author.resources.html.ref;
                                    nameref.target ="_blank";
                                    nameref.innerHTML = contentResponse[i].author.displayName;

                                    var spaceref = document.createElement('a');
                                    spaceref.href = contentResponse[i].parentPlace.html;
                                    spaceref.target ="_blank";
                                    spaceref.innerHTML = contentResponse[i].parentPlace.name;
                                    bodyspan.innerHTML = contentResponse[i].highlightSubject;
                                    ref.appendChild(bodyspan);
                                    refQ.appendChild(iconspan)
                                    var newDiv1 = document.createElement('div');
                                    newDiv1.style.marginLeft = "30px";
                                    var createdBy = document.createElement('p');
                                    var text = document.createTextNode("Created "+ days + " ago by ");
                                    createdBy.appendChild(text);
                                    createdBy.appendChild(nameref);
                                    var text1 = document.createTextNode(" in ");
                                    createdBy.appendChild(text1);
                                    createdBy.appendChild(spaceref);
                                    //createdBy.innerText = "Created by "  + nameref + "in " + spaceref ;
                                    var par = document.createElement('p');
                                    par.innerHTML =contentResponse[i].highlightBody;
                                    newDiv.style.padding = "25px 25px 0px 0px"
                                    newDiv.appendChild(refQ);
                                    newDiv1.appendChild(ref);
                                    newDiv1.appendChild(createdBy);
                                    newDiv1.appendChild(par);
                                    newDiv.appendChild(newDiv1);
                                    searchedContent.appendChild(newDiv);

                                    if(i==contentResponse.length -1){
                                        //gadgets.window.adjustHeight();
                                        showPage();
                                    }
                                }
                            }
                            else{
                                console.log("Else condtion");
                                for(i=(currentPage-1)*records_per_page;i<currentPage*records_per_page;i++){
                                    var iconspan = document.createElement('span');
                                    iconspan.className = "pwcmps_Icon " + contentResponse[i].iconCss;
                                    var newDiv = document.createElement('div');
                                    var bodyspan = document.createElement('span');
                                    var ref = document.createElement('a');
                                    var refQ = document.createElement('a');
                                    var days = numberofDays(contentResponse[i].updated);
                                    // var link = contentResponse[i].author.thumbnailUrl;
                                    // link = link.substring(0,35);
                                    ref.href  = contentResponse[i].resources.html.ref;
                                    ref.target ="_blank";

                                    refQ.href  = contentResponse[i].resources.html.ref;
                                    refQ.target ="_blank";

                                    var nameref = document.createElement('a');
                                    nameref.href = contentResponse[i].author.resources.html.ref;
                                    nameref.target ="_blank";
                                    nameref.innerHTML = contentResponse[i].author.displayName;

                                    var spaceref = document.createElement('a');
                                    spaceref.href = contentResponse[i].parentPlace.html;
                                    spaceref.target ="_blank";
                                    spaceref.innerHTML = contentResponse[i].parentPlace.name;
                                    bodyspan.innerHTML = contentResponse[i].highlightSubject;
                                    ref.appendChild(bodyspan);
                                    refQ.appendChild(iconspan)
                                    var newDiv1 = document.createElement('div');
                                    newDiv1.style.marginLeft = "30px";
                                    var createdBy = document.createElement('p');
                                    var text = document.createTextNode("Created "+ days + " ago by ");
                                    createdBy.appendChild(text);

                                    //var text2 = document.createTextNode("days ago by ");
                                    createdBy.appendChild(nameref);
                                    var text1 = document.createTextNode(" in ");
                                    createdBy.appendChild(text1);
                                    createdBy.appendChild(spaceref);
                                    //createdBy.innerText = "Created by "  + nameref + "in " + spaceref ;
                                    var par = document.createElement('p');
                                    par.innerHTML =contentResponse[i].highlightBody;
                                    newDiv.style.padding = "25px 25px 0px 0px"
                                    newDiv.appendChild(refQ);
                                    newDiv1.appendChild(ref);
                                    newDiv1.appendChild(createdBy);
                                    newDiv1.appendChild(par);
                                    newDiv.appendChild(newDiv1);
                                    searchedContent.appendChild(newDiv);

                                    if(i==currentPage*records_per_page -1){
                                        //gadgets.window.adjustHeight();
                                        showPage();
                                    }
                                }
                            }

                        }


                    }
                    else{
                        // showPage();
                        document.getElementById('noResult').style.display = "block";
                        document.getElementById("loader").style.display = "none";
                        document.getElementById('pagin').style.display = "none";

                        gadgets.window.adjustHeight();
                    }


                }
            }


        });

    }


}






function showPage() {
    //document.getElementById('noResult').style.display = "block";
    document.getElementById("loader").style.display = "none";
    document.getElementById("searchedContent").style.display = "block";
    document.getElementById('pagin').style.display = "block";

    gadgets.window.adjustHeight();
}




function showingDiv(){
    document.getElementById("intro-wrap").style.display = "none";
    document.getElementById("hideDiv8").className = "col-sm-8"
    document.getElementById("hideDiv4").style.display = "block"
    gadgets.window.adjustHeight();

}


function hidingDiv() {
    document.getElementById("intro-wrap").style.display = "block";
    document.getElementById("hideDiv8").className = "col-sm-11"
    document.getElementById("hideDiv4").style.display = "none"
    gadgets.window.adjustHeight();
}

function numberofDays(previousModified){
    var days;
    var currentDate = new Date().getTime();
    var modifieddate = new Date(previousModified).getTime();
    var ModifiedDays = currentDate - modifieddate;
    if(ModifiedDays/86400000 >=365){
         return "a year" ;
    }
    else{
        if(ModifiedDays/(86400000*30)>1){
            days = Math.ceil(ModifiedDays/(86400000*30));
            return days + " months"
        }
        else{
            days = Math.ceil(ModifiedDays/86400000);

            return days + " days"
        }

    }


}
function clearAll() {
    var flag =0;
    var authors = document.getElementById('Authors');
    var places = document.getElementById('places');
    var modifiedF =  document.getElementById('modified')
    for( var a =0;a<authors.children.length;a++ ){
        if(authors.children[a].children[0].children[0].checked == true){
            flag=1;
            authors.children[a].children[0].children[0].checked =false;
        }
    }
    for(var s=0;s<places.children.length;s++){
        if(places.children[s].children[0].children[0].checked == true){
            flag =1;
            places.children[s].children[0].children[0].checked = false;
        }
    }
    for(var m=0;m<modifiedF.children.length;m++) {
        if (modifiedF.children[m].children[0].children[0].checked == true) {
            flag = 1;
            modifiedF.children[m].children[0].children[0].checked =false;
        }
    }

    if(flag==1){searchQuery();}
    
}