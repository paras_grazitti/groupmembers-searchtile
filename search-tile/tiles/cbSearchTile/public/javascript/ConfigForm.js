var jiveBaseUrl =  "https://searchtile-bit9uex.rhcloud.com/cbSearchTile";

jive.tile.onOpen(function (config, options) {
    var SpaceResponse =[];
    // osapi.jive.core.get({
    //     v: "v3",
    //     href: '/places'
    // }).execute(function (response)  {
    //     console.log("places Result",response);
    //    // SpaceResponse.push(response.list)
    //     //response = JSON.parse(response);
    //     response.content.list.forEach(function(spaces){
    //         SpaceResponse.push(spaces);
    //
    //     });
    //     console.log("SpaceResponse",SpaceResponse);
    //     if(response.content.links.next != ""){
    //
    //        var nextResponse = getNextSpaces(response.content.links.next);
    //         console.log("nextResponse",nextResponse);
    //
    //         var places = document.getElementById('places');
    //         for(i=0;i<response.content.list.length;i++){
    //             var newDiv = document.createElement('div');
    //             // newDiv.className = "checkbox";
    //             var newOption = document.createElement('input');
    //             var label = document.createElement('label');
    //             label.className = "labelpadding";
    //             // var brtag = document.createElement('br');
    //             newOption.id = nextResponse.placeID;
    //             newOption.value = nextResponse.name;
    //             label.innerHTML = nextResponse.name;
    //             label.id = "A" + i;
    //             newOption.type = "checkbox";
    //             newDiv.appendChild(newOption);
    //             newDiv.appendChild(label);
    //
    //             places.appendChild(newDiv);
    //
    //
    //         }
    //     }
    //     else{
    //         var places = document.getElementById('places');
    //         for(i=0;i<response.content.list.length;i++){
    //             var newDiv = document.createElement('div');
    //             // newDiv.className = "checkbox";
    //             var newOption = document.createElement('input');
    //             var label = document.createElement('label');
    //             label.className = "labelpadding";
    //             // var brtag = document.createElement('br');
    //             newOption.id = response.content.list[i].placeID;
    //             newOption.value = response.content.list[i].name;
    //             label.innerHTML = response.content.list[i].name;
    //             label.id = "A" + i;
    //             newOption.type = "checkbox";
    //             newDiv.appendChild(newOption);
    //             newDiv.appendChild(label);
    //
    //             places.appendChild(newDiv);
    //
    //
    //         }
    //
    //     }
    //
    //
    //
    //     gadgets.window.adjustHeight();
    // });
    var request = osapi.jive.corev3.places.get({});
    processListByPage(request);

    function processListByPage(request) {
        //Execute the request to get a list
        request.execute(function(response) {
            if (response.error) {
                var code = response.error.code;
                var message = response.error.message;
                // present the user with an appropriate error message
            } else if (!response.list) {
                alert("Error: response is not a list!");
            } else {
                response.list.forEach(function(spaces){
                    SpaceResponse.push(spaces);
                    // console.log(SpaceResponse);

                });
                if (response.getNextPage) {
                    var requestNextPage = response.getNextPage();
                    processListByPage(requestNextPage);
                }
                else{
                    console.log("SpaceResponse",SpaceResponse)
                    var places = document.getElementById('places');
                    osapi.http.get({
                        href: jiveBaseUrl + "/configure/selectedSpaces",
                        'refreshInterval': 0,
                        format: 'json',
                        'authz': 'signed',
                        'headers': {
                            'Content-Type': ['application/json']
                        }

                    }).execute(function (getresponse) {
                        var j=0;
                        for(i=0;i<SpaceResponse.length;i++){
                            if(SpaceResponse[i].type != "blog" && SpaceResponse[i].type != "carousel" && SpaceResponse[i].type != "project" ){
                                if(SpaceResponse[i].name == getresponse.content.spaces[j] ){

                                    var newDiv = document.createElement('div');
                                    // newDiv.className = "checkbox";
                                    var newOption = document.createElement('input');
                                    var label = document.createElement('label');
                                    label.className = "labelpadding";
                                    // var brtag = document.createElement('br');
                                    newOption.checked = true;
                                    newOption.id = SpaceResponse[i].placeID;
                                    newOption.value = SpaceResponse[i].name;
                                    label.innerHTML = SpaceResponse[i].name;
                                    label.id = "A" + i;
                                    newOption.type = "checkbox";
                                    newDiv.appendChild(newOption);
                                    newDiv.appendChild(label);

                                    places.appendChild(newDiv);
                                    j++;
                                }
                                else{
                                    var newDiv = document.createElement('div');
                                    // newDiv.className = "checkbox";
                                    var newOption = document.createElement('input');
                                    var label = document.createElement('label');
                                    label.className = "labelpadding";
                                    // var brtag = document.createElement('br');
                                    newOption.checked = false;
                                    newOption.id = SpaceResponse[i].placeID;
                                    newOption.value = SpaceResponse[i].name;
                                    label.innerHTML = SpaceResponse[i].name;
                                    label.id = "A" + i;
                                    newOption.type = "checkbox";
                                    newDiv.appendChild(newOption);
                                    newDiv.appendChild(label);

                                    places.appendChild(newDiv);

                                }


                            }

                        }


                    });


                }
            }
        });
        gadgets.window.adjustHeight();
    }


});

function saveSpaces(){
    var spaces = [];
    var spaceId = [];
    var j=0;
    var spaceSelected = document.getElementById('places');
    for(i=0;i<spaceSelected.children.length;i++){
        if(spaceSelected.children[i].children[0].checked== true){
            spaces[j] = spaceSelected.children[i].children[0].value;
            spaceId[j] = spaceSelected.children[i].children[0].id;
            j++;

        }

    }
    console.log("Inside save Space function",spaces);
    osapi.http.post({
        href: jiveBaseUrl + "/configure/selectedSpaces",
        'refreshInterval': 0,
        format: 'json',
        'authz': 'signed',
        'headers': {
            'Content-Type': ['application/json']
        },
        'body':{
            'spaces': spaces,
            'spaceId':spaceId

        }
    }).execute(function (res) {
        console.log("Response for update",res);
        // if(res.content.status == 200)
        // {
        //     console.log("Response",res);
        //     // document.getElementById('invalidCredentials').innerHTML = res.content.message;
        //     // document.getElementById('invalidCredentials').style.display = "block";
        //     // changeVisibility();
        // }
    });

   jive.tile.close({});
}

